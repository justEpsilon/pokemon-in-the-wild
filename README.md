# PokemonTrainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Description 

This project is made as an assignament for Noroff Fullstack bootcamp. There are some redundant components, however they can be usefull for further represantation of the POKI-API, cuz there is so much potential. It is not entirely finished, however you can login and then choose pokemons you like (from the ones i have selected) who then will appear at your trainer page. its a SPA with router pages. Only local storage is used for information. all will be lost, when you reaload the APP. 