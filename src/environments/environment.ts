// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {

  production: false,
  userApiBAseUrl: 'http://localhost:3000',

  //LINK roots to Pokemon: 

  //ends in : offset=0&limit=20 
  // --limit shall stay the same, 
  // --offset increase every time by limit
  pokemon_ROOT:"https://pokeapi.co/api/v2/pokemon?",

  // just need to add x: number
  pokemonFeatures_URL: "https://pokeapi.co/api/v2/pokemon/",

  //need to add x.png
  pokemonThumbnail: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/",

  // need to add x.svg
  pokemonHD: "//raw.githubusercontent.com/PokeAPI/sprites/84204f8594790cfd04190a8d82beb31f49115c02/sprites/pokemon/other/dream-world/" 

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
