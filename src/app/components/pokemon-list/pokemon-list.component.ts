import { Component, OnInit } from "@angular/core";
import { PokemonAPI } from "src/app/api/pokemon.api";
import { LoginState } from "src/app/state/login.state";
import { Pokemon } from "../../models/pokemon.model";
import { PokemonsService } from "../../services/pokemons.services";
import { SelectedPokemonService } from "../../services/selected-pokemon.service";

@Component({
    selector: 'app-pokemon-list',
    templateUrl: './pokemon-list.component.html',
    styleUrls: ['./pokemon-list.component.css']
})

export class PokemonListComponent implements OnInit {

    constructor(
        private readonly pokemonService: PokemonsService,
        private readonly selectedPokemonService: SelectedPokemonService,
    ){

    }
    ngOnInit(): void {

        if (!this.pokemonService.isFetched){
            this.pokemonService.fetchPokemons();
            console.log("fetching pokemons");
        }
    }

    get pokemons(): Pokemon[] {
        return this.pokemonService.pokemons();
    }

    public handlePokemonClicked(pokemon:Pokemon): void {
        this.selectedPokemonService.setPokemon(pokemon)


    }
 }