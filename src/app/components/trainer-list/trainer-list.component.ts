import { Component, OnInit } from "@angular/core";
import { Pokemon } from "../../models/pokemon.model";
import { PokemonsService } from "../../services/pokemons.services";

@Component({
    selector: 'app-trainer-list',
    templateUrl: './trainer-list.component.html',
    styleUrls: ['./trainer-list.component.css']
})

export class TrainerListComponent implements OnInit {

    public pokemons: Pokemon[] = [];
    constructor(
        private readonly pokemonService: PokemonsService,
    ){

    }
    ngOnInit(): void {
        
        this.pokemons = this.pokemonService.pokemons()
    }

    // making a selected pokemon list. 
    get selectedPokemon(): Pokemon[] {
        return this.pokemons.filter(pokemon => pokemon.isSelected);
      }

 }