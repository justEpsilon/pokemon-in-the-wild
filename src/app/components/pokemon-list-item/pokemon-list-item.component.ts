// This is a child component to pokemon-list, we will use it so we manipulate out pokemons:D
import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Pokemon } from "../../models/pokemon.model";


@Component({
    selector: 'app-pokemon-list-item',
    templateUrl: "./pokemon-list-item.component.html",
    styleUrls: ['./pokemon-list-item.component.css']
})
export class PokemonListItemComponent {
    @Input() pokemon: Pokemon | undefined;
    @Output() clicked: EventEmitter <Pokemon> = new EventEmitter();


    // it is sending the clicked pokemon to pokemon selected component
    public onPokemonClicked(): void {
        console.log('pokemon list item is clicked');
        this.clicked.emit(this.pokemon);

        let pokemon = this.pokemon;

        if (pokemon != null && !pokemon.isSelected){
            pokemon.isSelected = true;

            // this.loginState.addCollectedPokemon(pokemon); // If I want to add it to my loging state, for later.

        } else if (pokemon != null && pokemon.isSelected){
            pokemon.isSelected = false;
        }



    }
}