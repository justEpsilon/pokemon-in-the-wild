import { Injectable } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { PokemonAPI } from '../api/pokemon.api';


// PokemonService to be shared across the components. 
@Injectable({
    providedIn: 'root'
})
// .this service now can be injected in any file,
// any component in your entire app, you can acsses this info easy.

// since i already made this, while testing with .json server, I am leving it in.
// pokemon.api can be used directly, and this component might be redundant for app this size. 

export class PokemonsService {

    // we want to update Pokemon in one place - service! 
    // them who access shall not brake it, but you know, people are wild, so keep it private.
    // public can just getPokemons, no putting allowed!

    private _pokemons: Pokemon[] = [];
    private _error: string = 'where are Pokemon?';
    public isFetched: boolean = false; 

   
    constructor(
        private readonly pokemonAPI: PokemonAPI
        ) {
    }

    public fetchPokemons(): void {
        //https://pokeapi.co/api/v2/pokemon/?offset=0&limit=20
        
        // also maybe good idea to leave this service since then I can add fetching extra pokemon here. 
        this.pokemonAPI.fetchPokemons$(100,0)// (limit, offset)
        .subscribe((pokemons:Pokemon[]) =>{
           this._pokemons = pokemons;
           this.isFetched = true; 
        })
        
    }

    // we need to expose Pokemons to public.  
    // getter type of function, the rest of it in another file.

    public pokemons(): Pokemon[] {
        return this._pokemons;
    }
    
    public error(): string {
        return this._error;
    }
    

}