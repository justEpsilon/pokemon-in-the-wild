import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonSelectedComponent } from './components/pokemon-selected/pokemon-selected.component';

import { CataloguePage } from './pages/catalogue/catalogue.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { LoginPage } from './pages/login/login.page';

import { AppRoutingModule } from './app-routing.module';
import { TrainerListComponent } from './components/trainer-list/trainer-list.component';


@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonSelectedComponent,
    CataloguePage, 
    PokemonListItemComponent,
    TrainerPage,
    LoginPage,
    TrainerListComponent
    
    // the 3 pages components : CataloguePage, TainerPage, LandingPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [], //should we put service providers?
  bootstrap: [AppComponent]
})
export class AppModule {
  
 }
