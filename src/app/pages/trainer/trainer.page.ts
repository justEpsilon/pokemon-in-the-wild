import { Component } from "@angular/core";
import { LoginState } from "src/app/state/login.state";
@Component({
    selector: 'app-trainer',
    templateUrl: './trainer.page.html',
    styleUrls: ['./trainer.page.css']
})
export class TrainerPage {

    constructor( 
        private readonly loginState: LoginState
    ){}
    
    public username =  this.loginState.getUsername();


}