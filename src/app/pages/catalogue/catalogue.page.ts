import { Component } from "@angular/core";  
import { LoginState } from "src/app/state/login.state";




@Component({
    selector: 'app-catalogue',
    templateUrl: './catalogue.page.html',
    styleUrls: ['./catalogue.page.css']
})
export class CataloguePage{

    constructor( 
        private readonly loginState: LoginState
    ){}
    
    public username =  this.loginState.getUsername();


}