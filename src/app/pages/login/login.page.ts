import { Component, Output } from "@angular/core";
import { NgForm } from "@angular/forms";
import { User } from "../../models/pokemon.model";
import { LoginState } from "src/app/state/login.state";

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.css']
})

export class LoginPage {

    userName: String = "";
    isRegistered: boolean = false; // this is for hiding and showing elements in html, (just playing with html). 
    
    constructor( 
        private readonly loginState: LoginState 
    ){}
    // so let me store a user login on input in local storage: 
    // --overwrite it everytime, unless it is the same username?, for simplicity
    // --store picked pokemon in there. 

    public onSubmit(createForm: NgForm){

        this.userName = createForm.value.username;

        this.loginState.setUser(createForm.value);
        console.log(createForm.value)
        console.log("login was submitted!!!")
        //console.log(this.userName)
        this.isRegistered = true;

    }

}