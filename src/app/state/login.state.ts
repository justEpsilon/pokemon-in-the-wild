import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Pokemon, User } from "../models/pokemon.model";

@Injectable({
    providedIn: 'root'
})
export class LoginState {
    // made collectedPokemon for storing picked pokemon, but turned out I don't need it. 
    // I can use it later if I actually make login&pass.

    private user$: BehaviorSubject<User> = new BehaviorSubject<User>({
        username: '',
        collectedPokemon: []
    });

    public setUser(username: string): void {
        let user: User = {
            username: username,
            collectedPokemon: []
        }       
        
        this.user$.next(user); 
        
    } 

    public getUsername(): String {
        const { username } = this.user$.getValue(); 
        console.log(username);
        return username;
    }

    public getUser$(): Observable<User> {
        return this.user$.asObservable();
    }

    public addCollectedPokemon(pokemon: Pokemon): void {
        const user = this.user$.getValue();
        user.collectedPokemon.push(pokemon); 

        this.user$.next(user);
    }

    // not really using it rn.
    // when new pokemons are fetched more than once, then this will be usefull, not to go trough giant lists. 
    public getCollectedPokemon(): Pokemon[] {
        let { collectedPokemon } = this.user$.getValue();
        return collectedPokemon; 
    }
}