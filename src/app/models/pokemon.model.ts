//Creating new types of objects, User and Pokemon, maybe will have to separate them. 
//for now just fetching from my local storage. 
export interface Pokemon {
    id: number;
    name: string;
    thumbnail: string; 
    imageHD: string;
    isSelected: boolean;
}

export interface User {
    username: string;
    collectedPokemon: Array<Pokemon>;
}

export interface PokemonImage {//maybe i need it
    large: string;
    medium: string;
    thumbnail: string;
  }

export interface PokemonResult {
    name: string;
    url: string;
}  

export interface PokemonResults {
    results: PokemonResult[];
}