import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

import { environment } from "src/environments/environment";
import { Pokemon, PokemonResults } from "../models/pokemon.model";

//https://pokeapi.co/api/v2/pokemon/?offset=0&limit=20

const { pokemon_ROOT, pokemonHD, pokemonThumbnail } = environment;

@Injectable({
    providedIn: 'root'
})
// We expose features of HttpClient as a property in our class: http
export class PokemonAPI {
    constructor(private readonly http: HttpClient){
    }  

    public fetchPokemons$(limit:number, offset:number): Observable<Pokemon[]>{

        let response =  this.http.get<PokemonResults>(`${pokemon_ROOT}offset=${offset}&limit=${limit}}`);

        return response.pipe(map( (results: PokemonResults) => {
            let pokemons = [];
            let id = offset + 1; 
            for (let result of results.results) {
                let pokemon: Pokemon  = {
                    name: result.name.toUpperCase(),
                    id: id,
                    thumbnail: pokemonThumbnail + id +".png",
                    imageHD: pokemonHD + id + ".svg",
                    isSelected: false
                }

                pokemons.push(pokemon);
                id += 1;
            }

            return pokemons;
        }))

    }
}

